﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonC
{
    class Program
    {
        public static void Main(string[] args)
        {
            StockProducto stock = new StockProducto();
            stock.CrearProductos();
            stock.ImprimirStockProductos();


            Cliente cliente = new Cliente();
            cliente.Apellidos = "Zambrano Zambrano";
            cliente.Nombres = "Michael Jackson";
            cliente.Email = "michael@web.com";
            cliente.Cedula = "1112223334";
            cliente.Contrasena = "mzambrano";

            Empresa empresa = new Empresa();
            empresa.RazonSocial = "Amazon";
            empresa.Direccion = "California";

            CabeceraFactura cabeceraCarrito = new CabeceraFactura();
            cabeceraCarrito.ClienteCabecera = cliente;
            cabeceraCarrito.EmpresaCabecera = empresa;

            Factura factura = new Factura();

            factura.Cabecera = cabeceraCarrito;
            string opcion;
            int codigo;
            do
            {
                Console.WriteLine("Ingrese el codigo del producto");
                codigo = int.Parse(Console.ReadLine());
                DetalleFactura detalleCarrito = new DetalleFactura();
                detalleCarrito.ProductoCarrito = stock.ListaStockProductos[codigo - 1];
                Console.WriteLine("Ingrese la cantidad del producto elegido:");
                detalleCarrito.Cantidad = int.Parse(Console.ReadLine());
                factura.Detalle.Add(detalleCarrito);
                Console.WriteLine("Escriba A para seguir agregando productoso S para salir");
                opcion = Console.ReadLine();
            } while (opcion != "S");

            //IMPRIMIR POR PANTALLA EL NOMBRE DEL PRODUCTO, SU PRECIO Y LA CANTIDAD
            Console.WriteLine("Productos facturados");
            Console.WriteLine("Descripcion\tPrecio\tCantidad");

            var detall = factura.Detalle.Select(p => p);
            foreach (var item in detall)
            {
                Console.WriteLine("{0}\t{1}\t{2}",
                    item.ProductoCarrito.Descripcion, item.ProductoCarrito.Precio, item.Cantidad);
            }


            Console.WriteLine();

            //Para ordenarlos por cantidad de mayor a menor
            var ordenados = factura.Detalle.OrderByDescending(c => c.Cantidad);
            foreach (var item in ordenados)
            {
                Console.WriteLine("{0}\t{1}\t{2}",
                    item.ProductoCarrito.Descripcion, item.ProductoCarrito.Precio, item.Cantidad);
            }

            var barato = factura.Detalle.OrderBy(b => b.ProductoCarrito.Precio).ToList();
            //Console.WriteLine(barato[0]);

            //List<DetalleFactura> baratos = new List<DetalleFactura>();


            int cont = 0; decimal x = 0;
            foreach (var item in barato)
            {
                if (cont == 0)
                {
                    x = item.ProductoCarrito.Precio;
                    Console.WriteLine("El o los más baratos son: ");
                    Console.WriteLine("{0}\t{1}\t{2}",
                        item.ProductoCarrito.Descripcion, item.ProductoCarrito.Precio, item.Cantidad);
                    cont++;
                }
                else
                {
                    if (x == item.ProductoCarrito.Precio)
                    {
                        Console.WriteLine("{0}\t{1}\t{2}",
                        item.ProductoCarrito.Descripcion, item.ProductoCarrito.Precio, item.Cantidad);
                    }
                }
                
            }
            
            /*
            var total = factura.Detalle.Sum(c => c.ProductoCarrito.Precio * c.Cantidad);
            Console.WriteLine();
            Console.WriteLine(total);
            */

            /*

            var resultado = factura.Detalle.Where(c => c.ProductoCarrito.Precio > 5).OrderBy(c => c.ProductoCarrito.Precio);
            foreach (var item in resultado)
            {

                Console.WriteLine("{0}\t\t{1}\t{2}",
                item.Identificador, item.Descripcion, item.Precio);

            }
            */

            factura.CalcularSubtotal();
            factura.CalcularDescuento();
            factura.CalcularTotal();

            Console.WriteLine(factura.SubTotal);
            Console.WriteLine(factura.Descuento);
            Console.WriteLine(factura.Total);

            Console.ReadKey();
        }
    }
}
